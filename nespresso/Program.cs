﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;


namespace nespresso
{
    class Program
    {
        public static Socket Create_Socket()
        {
            return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        }

        public static void Connect_Socket(Socket mySocket)
        {
            try
            {
                mySocket.Connect("51.91.120.237", 1212);
                Console.Out.WriteLine("Connexion Reussie!");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("échec de connexion: {0}", ex.ToString());
            }

        }

        public static string Recup_Trame(Socket mySocket)
        {
            try
            {
                byte[] buffer = new byte[512];
                mySocket.Receive(buffer);
                string carte = Encoding.ASCII.GetString(buffer);
                Console.Out.WriteLine("voici la trame  : {0}", carte);
                return carte;
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("recup impossible : {0}",ex.ToString());
                return null;
            }

        }


        public static void destroy_Socket(Socket mySocket)
        {
            try
            {
                mySocket.Shutdown(SocketShutdown.Both);
            }
            catch
            {
                mySocket.Close();
            }

        }

        public static int Main(string[] args)
        {
            Socket mySocket = Create_Socket();
            Connect_Socket(mySocket);
            string carte = Recup_Trame(mySocket);
            destroy_Socket(mySocket);
            return 0;
        }
    }
}